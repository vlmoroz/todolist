const listEl = document.querySelector(".js-list");
const addButtonEl = document.querySelector(".js-add-button");
const checkBoxEl = document.querySelector('.form-check-input');
const jsRemove = document.querySelector('.js-remove');

addButtonEl.addEventListener("click", () => {
	const inputEl = document.querySelector(".js-input");
	const inputValue = inputEl.value;
	inputEl.value = '';

	const liEl = getItemEl(inputValue);
	listEl.append(liEl);
});

listEl.addEventListener(
	'click',
	(event) => {
		if(event.target.classList.contains('js-checkbox')){
			event.target.closest('.form-check').classList.toggle('done')
		}
		
		if(event.target.classList.contains('remove')){
			event.target.closest('.list-group-item').remove();
		}
	}
)

function getItemEl(text) {
	const liEl = document.createElement("li");
	liEl.classList.add("list-group-item");

	const formCheckEl = document.createElement("div");
	formCheckEl.classList.add("form-check");

	const checkBoxEl = document.createElement("input");
	checkBoxEl.classList.add("form-check-input");
	checkBoxEl.classList.add("js-checkbox");
	checkBoxEl.setAttribute("type", "checkbox");

	const spanEl = document.createElement("span");
	spanEl.classList.add("js-item-content");
	spanEl.textContent = text;

	const removeBtnEl = document.createElement("div");
	removeBtnEl.classList.add("remove");
	removeBtnEl.classList.add("js-remove");
	removeBtnEl.textContent = "x";

	formCheckEl.append(checkBoxEl);
	formCheckEl.append(spanEl);

	liEl.append(formCheckEl);
	liEl.append(removeBtnEl);

	return liEl;
}
